require "block"
require "arena"
require "consts"

local tetris_arena = arena:createArena()

local L = tetromino:newL()

function love.load()
    love.window.setMode(winW, winH+20)
    arena.controller:addTetromino(L, tetris_arena)
end

function love.update(dt)
end

function love.keypressed(key, scancode, isrepeat)
    if key == 'i' then
        for _,b in ipairs(L.blocks) do
            arena[b.x][b.y] = 1
        end
    end
    --if key == 'a' then
    --    arena:controllerTetromino(L, "left")
    --end
    if key == 'a' then
        -- arena.controller:removeTetromino(L)
        -- tetromino:moveRight(L)
        -- arena.controller:addTetromino(L)
        arena.controller:removeTetromino(L, tetris_arena)
        arena.controller:addTetromino(L, tetris_arena)
        -- arena:print(tetris_arena)
    end
    if key == "w" then
        arena.controller:removeTetromino(L, tetris_arena)
        --arena:print(tetris_arena)
    --    arena.controller:removeTetromino(L)
    --    tetromino:rotateRight(L)
    --    arena.controller:addTetromino(L)
    end
    if key == "d" then
        arena.controller:removeTetromino(L, tetris_arena)
        L.index.x = L.index.x + 1
        arena.controller:addTetromino(L, tetris_arena)
    end
end

function love.draw()
    -- drawNextBlockSquare()
    arena.draw:drawAll(tetris_arena)
end

