require "consts"

arena = {}
arena.draw = {}
arena.controller = {}

function arena:createArena()
    local ta = {}
    for _=1,20 do
        local y = {}
        for _=1,10 do
            table.insert(y, 0)
        end
        table.insert(ta, y)
    end
    return ta
end

function arena:print(tetris_arena)
    if tetris_arena then
        print("exists")
    end
    for _,v in ipairs(tetris_arena) do
        for _,y in ipairs(v) do
            print("y " .. y)
        end
    end
end

function arena.draw:drawArena()
    GPS.line(leftLimitX, 0, leftLimitX, winH)
    GPS.line(rightLimitX, 0, rightLimitX, winH)
end

function arena.draw:drawNextBlockSquare()
    GPS.rectangle('line', rightLimitX+10, 0, 150, 150)
end

function arena.draw:drawAll(tetris_arena)
    for ri,r in pairs(tetris_arena) do
        for hi,h in pairs(r) do
            if h == 1 then
                GPS.rectangle('line', leftLimitX+20*hi, 20*ri, brkW, brkH)
            end
        end
    end
end

function arena.controller:addTetromino(tetrimo, tetris_arena)
    -- TODO: Make this work
    local bls = tetrimo.blocks
    if not bls then return -1 end

    local threeXthree = function ()
        for y=1,3 do
            for x=1,3 do
                if bls[y][x] == 1 then
                    local ind = tetrimo.index
                    tetris_arena[ind.y+(y-2)][ind.x+(x-2)] = 1
                end
            end
        end
    end

    local fourXfour = function ()
    end
    local square = function ()
    end

    threeXthree()

    local threes = {L, "J", "S", "Z", "T"}
    if threes[tetrimo.type] then -- TODO: Fix: This is never true
        print("AAAAAAAAAAA")
    else
        return -1
    end
end

function arena.controller:removeTetromino(tetrimo, tetris_arena)
    -- TODO: make this work
    local bls = tetrimo.blocks
    if not bls then return -1 end

    local threeXthree = function ()
        for y=1,3 do
            for x=1,3 do
                if bls[y][x] == 1 then
                    local ind = tetrimo.index
                    tetris_arena[ind.y+(y-2)][ind.x+(x-2)] = 0
                end
            end
        end
    end

    local fourXfour = function ()
    end
    local square = function ()
    end

    threeXthree()

    local threes = {L, "J", "S", "Z", "T"}
    if threes[tetrimo.type] then -- TODO: Fix: This is never true
        print("AAAAAAAAAAA")
    else
        return -1
    end
end

function arena:controllerTetromino(tetrimo, move)
    arena:removeTetromino(tetrimo)
    if move == "left" then
        block:moveLeft(tetrimo)
    elseif move == "right" then
        block:moveRight(tetrimo)
    end
    arena:addTetromino(tetrimo)
end
