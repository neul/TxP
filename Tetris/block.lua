require "consts"

tetromino = {}
-- mutate functions
function tetromino:removeLast(tetrimo)
    local blocks = tetrimo["blocks"]
    if blocks then
        for i,_ in ipairs(blocks) do
            if i == tetrimo.bcount and tetrimo.bcount > 0 then
                blocks[i] = nil
                tetrimo.bcount = tetrimo.bcount-1
            end
        end
    end
end

function tetromino:newI()
    return {
        blocks = {
            {0, 0, 0, 0},
            {1, 1, 1, 1},
            {0, 0, 0, 0},
            {0, 0, 0, 0},
        },
        bcount = 4,
        rotation = "ver",
        type = "I"
    }
end

function tetromino:newJ()
    -- TODO: 
    -- Have an 3x3 array and origin point
    -- check the limits by origin and rotate by origin
    return {
        blocks = {
            {0, 1, 0},
            {0, 1, 0},
            {1, 1, 0},
        },
        origin = {x=1, y=1},
        bcount = 4,
        type = "J"
    }
end

function tetromino:newL()
    return {
        blocks = {
            {1, 0, 0},
            {1, 1, 1},
            {0, 0, 0},
        },
        index = {x=2,y=2},
        bcount = 4,
        type = L
    }
end

function tetromino:newO()
    return {
        blocks = {
            [1] = {x = 5, y = 1},
            [2] = {x = 6, y = 1},
            [3] = {x = 5, y = 2},
            [4] = {x = 6, y = 2}
        },
        bcount = 4,
        rotation = "hor",
        type = "O"
    }
end

function tetromino:newT()
    return {
        blocks = {
            [1] = {x = 5, y = 1},
            [2] = {x = 4, y = 2},
            [3] = {x = 5, y = 2},
            [4] = {x = 6, y = 2}
        },
        bcount = 4,
        rotation = "hor",
        type = "T"
    }
end

function tetromino:newS()
    return {
        blocks = {
            [1] = {x = 5, y = 2},
            [2] = {x = 6, y = 2},
            [3] = {x = 6, y = 1},
            [4] = {x = 7, y = 1}
        },
        bcount = 4,
        rotation = "hor",
        type = "S"
    }
end

function tetromino:newZ()
    return {
        blocks = {
            [1] = {x = 4, y = 1},
            [2] = {x = 5, y = 1},
            [3] = {x = 5, y = 2},
            [4] = {x = 6, y = 2},
        },
        bcount = 4,
        rotation = "hor",
        type = "Z"
    }
end
---
function tetromino:checkIfInRange(tetrimo, move)
    local switch = function ()
        tetrimo = tetrimo or nil
        if tetrimo == nil then return -1 end
        local blocks = tetrimo.blocks
        local case =
        {
            ["O"] = function ()
            end,
            ["I"] = function ()
            end,
            ["L"] = function ()
                return true
            end,
            --["J"],
            --["S"],
            --["Z"],
            --["T"] = function ()
            --end,
            ["default"] = function ()
                print("default at switch")
                return false
            end
        }

        if case[tetrimo.type] then
            return case[tetrimo.type]()
        else
            return case["default"]()
        end
    end
    return switch(tetrimo)
end

-- rotation
function tetromino:rotateRight(tetrimo)
    -- TODO MAKE THIS WORK
    local blocks = tetrimo.blocks
    if not blocks then return -1 end
    if not blocks[1] then return -1 end

    local degree90rotatedM = {}
    for x=1,#blocks[1] do
        print("DID ENTER FIR")
        local ax = {}
        for y=#blocks,1,-1 do
            table.insert(ax, blocks[y][x])
        end
        table.insert(degree90rotatedM, ax)
    end

    print("DID ENTER SEC")
    for y=1,#degree90rotatedM do
        for x=1,#degree90rotatedM[0] do
            print(degree90rotatedM[y][x])
        end
    end
    tetrimo.blocks = degree90rotatedM
end

-- move funcs 
function tetromino:moveRight(tetrimo)
    if tetromino:checkIfInRange(tetrimo, "r") then
        for _,b in ipairs(tetrimo["blocks"]) do
            if b.x <= 9 then
                b.x = b.x + 1
            end
        end
    end
end
function tetromino:moveLeft(tetrimo)
    if tetromino:checkIfInRange(tetrimo, "l") then
        for _,b in ipairs(tetrimo["blocks"]) do
            b.x = b.x - 1
        end
    end
end
function tetromino:moveUp(tetrimo)
    if tetromino:checkIfInRange(tetrimo, "u") then
        for _,b in ipairs(tetrimo["blocks"]) do
            if b.y >= 2 then
                b.y = b.y - 1
            end
        end
    end
end
function tetromino:moveDown(tetrimo)
    if tetromino:checkIfInRange(tetrimo, "d") then
        for _,b in ipairs(tetrimo["blocks"]) do
            if b.y <= 19 then
                b.y = b.y + 1
            end
        end
    end
end
