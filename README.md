# The Game
2 Player game which tetris meets with pong. One person controls the tetris and the other person tries to destroy the tetris blocks by controlling the pong part.

# TODOs:
## Core:
    + Represent all the blocks on the area matrix
    + For spawning blocks first:
        + Create a array with tetrimos
        + Select a random index from that array for 7 times
        + Create the next blocks section
        + When you spawned all of them cycle goes continues
    + You need a one section for holding
## Later
    + Have a mode for selecting spawned blocks
