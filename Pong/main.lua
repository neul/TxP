local KBD = love.keyboard
local GPS = love.graphics

local windowHeight = GPS.getHeight()
local windowWidth = GPS.getWidth()
local bckH = 50
local bckW = 10

local bricket_1 = {
    x = 10,
    y = windowHeight / 2 - bckH
}

local bricket_2 = {
    x = windowWidth - 20,
    y = windowHeight / 2 - bckH
}

local ball = {
    x = windowWidth/2,
    y = windowHeight/2,
    vx = 2,
    vy = 2,
    radius = 5
}

local a_score = 0
local b_score = 0

function love.load()
    -- GPS.setMode(width, height)
    local rvm = ball:randomVecMove()
    ball.vx = rvm.x * ball.vx
    ball.vy = rvm.y * ball.vy
end

function love.update(dt)
    roundBrickPos(bricket_1)
    roundBrickPos(bricket_2)
    brickMovement(bricket_1, dt)
    brickMovement(bricket_2, dt)

    if ball.x >= windowWidth - ball.radius/2 then
        a_score = a_score + 1
        ball:reset()
    elseif ball.x <= 0 + ball.radius/2 then
        b_score = b_score + 1
        ball:reset()
    end

    if ball.y - ball.radius <= 0 then
        ball.vy = -ball.vy
    elseif ball.y + ball.radius >= windowHeight then
        ball.vy = -ball.vy
    end

    if ball:inBricketRangeBr2() then
        ball.vx = -ball.vx
    elseif ball:inBricketRangeBr1() then
        ball.vx = -ball.vx
    end

    if KBD.isDown("f") then
        local rvm = ball:randomVecMove()
        ball.vx, ball.vy = 2, 2
        ball.vx = rvm.x * ball.vx
        ball.vy = rvm.y * ball.vy
    end

    ball.x = ball.x + ball.vx
    ball.y = ball.y + ball.vy
end

function love.draw()
    GPS.rectangle("fill", bricket_1.x, bricket_1.y, bckW, bckH)
    GPS.rectangle("fill", bricket_2.x, bricket_2.y, bckW, bckH)

    GPS.circle('line', ball.x, ball.y, ball.radius)
end

function brickMovement(brick, dt)
    if brick.y >= 0 and brick.y <= 550 then
        if KBD.isDown("w") then
            brick.y = brick.y - 150 * dt
        elseif KBD.isDown("s") then
            brick.y = brick.y + 150 * dt
        end
    end
end

function roundBrickPos(brick)
    if brick.y < 0 then
        brick.y = 0
    elseif brick.y > 550 then
        brick.y = 550
    end
    if brick.x < 0 then
        brick.x = 0
    end
end

function ball:randomVecMove()
    math.randomseed(os.time())
    local rx = 1
    local ry = math.random(-1, 1)
    return {
        x = rx,
        y = ry
    }
end

function ball:reset()
    ball.vx, ball.vy = 0, 0
    ball.x = windowWidth/2
    ball.y = windowHeight/2
end

function ball:inBricketRangeBr1()
    return
    ball.x - ball.radius <= bricket_1.x + bckW and
    ball.y >= bricket_1.y and
    ball.y <= bricket_1.y+bckH
end

function ball:inBricketRangeBr2()
    return
    ball.x + ball.radius >= bricket_2.x and
    ball.y >= bricket_2.y and
    ball.y <= bricket_2.y+bckH
end
